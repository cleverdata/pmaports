# Maintainer: Oliver Smith <ollieparanoid@postmarketos.org>
# Co-Maintainer: Clayton Craft <clayton@craftyguy.net>
pkgname=postmarketos-mkinitfs
pkgver=2.0
pkgrel=0
pkgdesc="Tool to generate initramfs images for postmarketOS"
url="https://postmarketos.org"
depends="
	boot-deploy>=0.7
"
makedepends="go scdoc"
replaces="mkinitfs"
triggers="$pkgname.trigger=/usr/share/mkinitfs/*:/usr/share/kernel/*:/usr/share/mkinitfs-triggers"
# mkinitfs-vendor-$pkgver.tar.gz: vendored Go deps, is part of the release:
# https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/releases
source="
	https://gitlab.com/postmarketOS/postmarketos-mkinitfs/-/archive/$pkgver/postmarketos-mkinitfs-$pkgver.tar.gz
	https://gitlab.com/api/v4/projects/postmarketOS%2Fpostmarketos-mkinitfs/packages/generic/mkinitfs-vendor-$pkgver/$pkgver/mkinitfs-vendor-$pkgver.tar.gz
	"

install="$pkgname.post-upgrade"
arch="all"
license="GPL-2.0-or-later"
provider_priority=999  # higher priority than Alpine's mkinitfs
provides="initramfs-generator"
subpackages="$pkgname-doc"

export CGO_ENABLED=0
export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"
export GOPATH="$srcdir"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"

prepare() {
	default_prepare
	ln -s "$srcdir"/vendor "$builddir"/vendor
}

build() {
	unset LDFLAGS  # passed to Go as linker flags, which are invalid
	make
}

package() {
	make PREFIX=/usr DESTDIR="$pkgdir" install
}

check() {
	go test ./...
}

sha512sums="
c401902f9adc2c8dbefa31c2eda4b33be678fe3db103e5e868c4a4b8850aff4fffcbd4d922d3ab1a27aee956ed25ed26671217e689d8f7bed6dc422dfe0ec98d  postmarketos-mkinitfs-2.0.tar.gz
185074152b7fba098a9fe10253cb5a33c57d3da5f6b657cdde2c599a9acc94bb1771d8dd06478af4765e344ff156bbe9fa8a240f007096e411d447baeb744388  mkinitfs-vendor-2.0.tar.gz
"
